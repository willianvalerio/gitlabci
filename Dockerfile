FROM node:8.11.4

WORKDIR /app/website

EXPOSE 3000 35729
COPY ./docs /app/docs
COPY ./website /app/website
CMD ["npm", "install", "-g", "broken-link-checker"]
RUN yarn install

CMD ["yarn", "start"]
